mixin Parent {
	
	void m1(){
		print("In m1 Parent");
	}

}
class Demo {

	void m1(){
		print("im m2 Demo");
	}
}
class Child extends Demo with Parent{

	
}
void main(){
	Child obj = new Child();
	obj.m1();
	obj.m1();
}
