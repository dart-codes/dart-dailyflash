class Parent{

	void m1(){
		print("In m1 Parent");
	}
}
mixin Demo1 on Parent{
	
	void fun1(){
		print("In Demo1 Fun");
	}	
	
}
/*
mixin Demo2 {

	void fun2(){
		print("In Demo2 fun2");
	}
	
}*/
class Demo extends Parent with Demo1 {

}
void main(){

	Demo obj = new Demo();
	obj.m1();
	obj.fun1();
}
