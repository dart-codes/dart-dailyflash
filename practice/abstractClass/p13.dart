mixin Demo1{
	void fun1(){
		print("In Demo1 Fun");
	}	
	
}
mixin Demo2 {

	void fun2(){
		print("In Demo2 fun2");
	}
	
}
class Parent with Demo1,Demo2{

}
class Demo extends Parent {

}
void main(){

	Demo obj = new Demo();
	obj.fun1();
	obj.fun2();
}
