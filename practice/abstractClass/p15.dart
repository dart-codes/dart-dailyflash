abstract mixin class Demo{
	
	void fun1(){
		print("In Demo Fun");
	}
	void fun2();
}
class child{
	void m1(){
		print("In m1");
	}
}
class Demo1 extends child with Demo{

	void fun2(){
		print("In Fun2");
	}
}
void main(){

	Demo1 obj = new Demo1();
	obj.fun1();
	obj.fun2();
	obj.m1();
}
