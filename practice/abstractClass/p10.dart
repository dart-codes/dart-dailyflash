abstract class Demo1{
	
	void fun1(){
		print("In Demo1 Fun");
	}	
	void fun2();
}
abstract class Demo3{

	void fun3(){
		print("In Demo2 fun3");
	}
	void fun4();
}
class Demo implements Demo1, Demo3{

	fun2(){
		print("In Demo fun2");
	}
	fun4(){
                print("In Demo fun4");
        }
}
void main(){

	Demo obj = new Demo();
	obj.fun1();
	obj.fun2();
	obj.fun3();
	obj.fun4();
}
