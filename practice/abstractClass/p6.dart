abstract class Interface1{

	void m1(){

		print("In interface 1");	
	}	
}
abstract class Interface2{

	void m2(){
		print("In interface 2");
	}
}
class demo implements Interface1,Interface2{

	void m1(){
		print("In Demo m1");
	}
	void m2(){
		print("In Demo m2");
	}
}
void main(){

	demo obj = new demo();
	obj.m1();
	obj.m2();
}
