mixin Demo1{
	int x =10;
	void fun1(){
		print("In Demo1 Fun");
	}	
	void fun2();
}

class Demo with Demo1{

	fun2(){
		print("In Demo fun2");
		print("x = $x");
	}
}
void main(){

	Demo1 obj = new Demo();
	obj.fun1();
	obj.fun2();
}
