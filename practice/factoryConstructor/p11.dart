class Backend{

	String? lang;
	Backend._code(String lang){
		if(lang =='js'){
			this.lang = 'Node JS';
		}else if(lang == 'java'){
			this.lang = "SpringBoot";
		}else{
			this.lang = "Node JS / SpringBoot";
		}
		
	}
	factory Backend(String lang){
		return Backend._code(lang);	
	}
}
