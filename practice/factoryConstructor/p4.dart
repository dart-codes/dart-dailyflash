class Demo{

	Demo(){

		print("Normal Constructor");
	}
	Demo.Const1(){
		print("Named Constructor 1");
	}
	Demo.Const2(){
		print("Named Constructor 2");
	}
}

void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo.Const1();
	Demo obj3 = new Demo.Const2();

}
