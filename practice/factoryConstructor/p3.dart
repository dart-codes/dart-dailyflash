class Demo{
	
	int? x;
	String? name;
	Demo(this.x,this.name){
		print("In parameterized constructor");
	}
}

void main(){

	Demo obj = new Demo(10,"Mayur");
}
