class Demo{

	final int? x;
	final String? name;
	
	const Demo(this.x,this.name);
}
void main(){

	Demo obj1= const Demo(10,"Mayur");
	Demo obj2 = const  Demo(20,"Pooja");

	print(obj1.hashCode);
	print(obj2.hashCode);
}
