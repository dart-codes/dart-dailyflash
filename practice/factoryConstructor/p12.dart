abstract class Developer{

	factory Developer(String lang){
		if(lang == "Backend"){
			return Backend();
		}else if(lang == "Frontend"){
                        return Frontend();
                }else if(lang == "Mobile"){
                        return Mobile();
                }else{
			return others();
		}
	}
	void devlang();
} 
class Backend implements Developer{
	void devlang(){
		print("NodeJs/SpringBoot");
	}
}
class Frontend implements Developer{
        void devlang(){
                print("ReactJs/AngularJs");
        }
}
class Mobile implements Developer{
        void devlang(){
                print("Flutter/Android");
        }
}
class others implements Developer{
        void devlang(){
                print("devops/testing/support");
        }
}

void main(){

	Developer obj1 = new Developer("Frontend");
	obj1.devlang();
	Developer obj2 = new Developer("Backend");
        obj2.devlang();
	Developer obj3 = new Developer("Mobile");
        obj3.devlang();
	Developer obj4 = new Developer("others");
        obj4.devlang();
}
