import 'dart:io';
void main(){

	print("Start Main");
	print("Enter Value");
	try{
		int? val = int.parse(stdin.readLineSync()!);
		print(val);
	}catch(ex){
		print(ex);
	}on IntegerDivisionByZeroException{
	
		print("IntegerDivisionByZero Eception handled");

	}on FormatException{
		print("Format Exception Handled");
	}
	catch(ex){
		print(ex);
	}
	
	print("End Main");
}
