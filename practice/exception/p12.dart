import 'dart:io';

class NotProfitable implements Exception{

	String str;
	NotProfitable(this.str);

	String toString(){
		return str;
	}
}
void main(){

	int empCount = int.parse(stdin.readLineSync()!);
	String? name = stdin.readLineSync();
	int profit = int.parse(stdin.readLineSync()!);
	try{
		if(profit < 0){
			throw new NotProfitable("loss");
		}		
	}catch(ex){

		print(ex.toString());
	}
	print("$name $empCount $profit");
}
