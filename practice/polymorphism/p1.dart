class Demo{

	int x = 10;
	void fun1(){
		print("In Fun");
		print(x);
	}

}

class DemoChild extends Demo{

	int x = 20;
	void fun1(){
		print("In Child Fun");
		print(x);
	}
}

void main(){

	DemoChild obj = new DemoChild();
	obj.fun1();

	Demo obj1 = new Demo();
	obj1.fun1();

	Demo obj2 = new DemoChild();
	obj2.fun1();
	
}
