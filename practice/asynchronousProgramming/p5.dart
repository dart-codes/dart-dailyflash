import 'dart:io';

Future<String?> getOrderName() async{

	print("Enter Item Name");
	await Future.delayed(Duration(seconds:2),()=> print("2 sec halt"));
	String? name = stdin.readLineSync();
	return name;
}

Future<String?> getOrder() async{

	return Future.delayed(Duration(seconds:5),()=> getOrderName());
}

Future<String> getOrderMessage() async{

	var order = await getOrder();
	return "Your Order is $order";
}

Future<void> main() async{

	print("start");
	print(await getOrderMessage());
	print("End");
}
