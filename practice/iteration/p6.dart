

class proLang implements Iterator{

	int index = -1; 
	var prolang = [];

	proLang(String lang){

		this.prolang = lang.split(",");
	}

	bool moveNext(){
		
		if(index < prolang.length-1){
			index++;
			return true ;
		}
		return false;
	}
	get current{
		if(index >=0 && index <= prolang.length-1){
			return prolang[index];
		}
	}
}
void main(){

	proLang obj = new proLang("CPP,JAVA,PYTHON,DART");
	while(obj.moveNext()){
		print(obj.current);	
	}
}

