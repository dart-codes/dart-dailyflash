

class Emp implements Iterator{

	int index = -1; 
	var empId = [];
	var empName = [];

	Emp(String empid,String empname){

		this.empId = empid.split(",");
		this.empName = empname.split(",");
	}

	bool moveNext(){
		
		if(index < empId.length-1){
			index++;
			return true ;
		}
		return false;
	}
	get current{
		if(index >=0 && index <= empId.length-1){
			return "${empId[index]} : ${empName[index]}";
		}
	}
}
void main(){

	Emp obj = new Emp("1,2,3,4,5","MAYUR,POOJA,YASH,SANSKAR,BAHETI");
	while(obj.moveNext()){
		print(obj.current);	
	}
}

