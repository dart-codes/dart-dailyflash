void main(){

	var players = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik","Ravindra"];
	
	//any() - checks whether any element of this iterable satisfies
	var rv = players.any((player)=>player[0] == "R");
	print(rv);  //true

	var length = players.any((player)=>player.length<=5);
	print(length); //true

	
	//contains - whether the collection contains an element equal to element
	var rt = players.contains("Rohit");
	print(rt);  //true	

	//elementAt(int index) - return element of that index
	var EA = players.elementAt(3);
	print(EA); //KL Rahul

	//every() - checks whether every element of this iterable satisfies test
	var Every = players.every((player)=>player[0] == "R");
	print(Every); //false

	var Every1 = players.every((player)=>player.length >= 4);
        print(Every1); //true 

	//firstWhere() - first element that satisfies the given predicate test
	var fw = players.firstWhere((player)=>player[0] == "S");
	print(fw); //Shubman

	//lastWhere() - last element that satisfies the given predicate test
        var lw = players.lastWhere((player)=>player[0] == "S");
        print(lw); //Shreyas
	

	//fold() - reduces collection to single value by iteratively combining each element of the collectionwith exsting value
	var initval = "";
	var f = players.fold(initval,(preval,player)=>preval+player);
	print(f);
	print(f.runtimeType);

	//followedBy() - creates the lazy concatention of this iterable & others
	var fo = players.followedBy(["Ravindra","Bumrah"]);
	print(fo);
	print(players);

	//forEach() - invokes action on each element of this iterable in iteration order
	players.forEach(print);

	//join() - joins each element 
	print(players.join("->"));

	//map - the current element of this itarabe modified by toElement
	var map = players.map((player)=>player+" - IND");
	print(map);

	//reduce() - works same as fold
	var r = players.reduce((value,player)=>value + player);
	print(r);

	//singleWhere() - 
	var Sw = players.singleWhere((player)=>player.length == 6);
	print(Sw);	

	//skip() - 
	var skip = players.skip(4);
	print(skip);

	
	//skipWhile() - 
	var Sk = players.skipWhile((player)=>player[0] == "V");
	print(Sk);

	//take() - 
	var take = players.take(5);
	print(take);

	//takeWhile() -
	var tW = players.takeWhile((player)=>player[0] == "R");
	print(tW); 

	//toList()
	print(players.toList());

	//toSet();
	print(players.toSet());

	//where
	print(players.where((player)=>player[0] == "S"));

	
}
