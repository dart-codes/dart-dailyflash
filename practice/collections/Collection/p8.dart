void main(){

	List player = ["Yash", "Pooja", "Mayur", "Sanskar", "Rishabh"];
	
	print(player);	//player list
	
	print(player.first);	//yash
	
	print(player.hashCode); //100
	
	print(player.isEmpty); //false
	
	print(player.isNotEmpty); //true

	print(player.iterator);	//

	print(player.last); //rishabh

	print(player.length); // 5

	print(player.reversed); // reversed playerList

	print(player.runtimeType); //List<dynamic>

	print(player.single);	//
}
