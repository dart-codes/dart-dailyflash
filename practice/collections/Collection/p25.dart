

import 'dart:collection';

final class Company extends LinkedListEntry<Company>{

	int empCount;
	String Companyname;
	double Revenue;
	
	Company(this.empCount,this.Companyname,this.Revenue);
	
	String toString(){

		return "$empCount - $Companyname - $Revenue";
	}
}
void main(){

	var company = LinkedList<Company>();

	company.add(Company(35,"Vishwakarma", 10000.00));
	company.add(Company(800,"TCS", 185000.00));
	company.add(Company(350,"Infosys", 1650000.00));
	company.add(Company(3780,"Cognizant", 1047500.00));

	print(company);
	company.first.unlink();
	print(company);
}
