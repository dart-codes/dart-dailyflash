void main(){

	var prolang = List.empty(growable:true);

	prolang.add("java");
	prolang.add("python");
	prolang.add("dart");
	prolang.add("java");

	print(prolang);
	
	var lang = ["ReactJS","Springboot","Flutter"];

	prolang.addAll(lang);
	print(prolang);
	prolang.insert(3,"CPP");
	print(prolang);
	prolang.insertAll(3,["GO","Swift"]);
	print(prolang);
	prolang.replaceRange(3,7,["dart"]);
	print(prolang);
	
	prolang.remove("java");
	print(prolang);
	prolang.removeAt(3);
	print(prolang);
	prolang.removeLast();
	print(prolang);
	prolang.removeRange(0,1);
	print(prolang);
	prolang.removeWhere((lang)=> lang.startsWith("d"));
	print(prolang);
	prolang.clear();
	print(prolang);
}
