void main(){

	List player1 = List.empty(); 	//fixed length
	List player2 = List.empty(growable:true); 	//growable length
	print(player2);
//	player1.add("Pooja");  - error - fixedlength
//	player2[0] = "Mayur";	-error - zero index
	player2.add("Pooja");
	player2.add("Mayur");
	print(player2);
	player2[1] = "Mayur Vishwakarma";
	print(player2);
}
