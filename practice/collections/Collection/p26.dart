import 'dart:collection';

void main(){

	var compData = Queue<String>();

	compData.add("Microsoft");
	compData.add("Amazon");
	compData.add("Google");

	print(compData);
	print(compData.runtimeType);
	compData.addFirst("Apple");
	print(compData);
	compData.addLast("Veritas");
	print(compData);
}
