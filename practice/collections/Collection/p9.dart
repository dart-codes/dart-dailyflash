void main(){

	var prolang = List.empty(growable:true);

	prolang.add("java");
	prolang.add("python");
	prolang.add("dart");
	prolang.add("java");

	print(prolang);

	print(prolang[0]);	//python	//Accesss by Index
	print(prolang.elementAt(2));	//dart
	print(prolang.getRange(0,3));	//(java,python,dart)
	print(prolang.indexOf("dart"));	//2
	print(prolang.indexWhere((lang)=>lang.startsWith("d")));
	print(prolang.lastIndexOf("java")); //3
	
	
}
