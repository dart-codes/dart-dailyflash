import 'dart:io';

void main() async {

	File f = new File("Mayur.txt");

	print(f.absolute);

	print(f.path);

	final lastAccesedDate = await f.lastAccessed();
	print(lastAccesedDate);

	final lastModifiedDate = await f.lastModified();
	print(lastModifiedDate);

	final Length = await f.length();
	print(Length);

	final exits = await f.exists();
	print(exits);
	
}
