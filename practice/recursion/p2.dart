void fun(int x){

        print(x);
        x--;
        if(x>0){
                fun(x);
        }
}
void gun(int a){

        if(a==0){
                return;
        }
        print(a--);
        fun(a);
}

void main(){
        print("by for loop");
        for(int i=5;i>=1;i--){
                print(i);
        }
        print("by recursion 1");
        fun(5);

        print("by recursion 2");
        gun(5);

}
