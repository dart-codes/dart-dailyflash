int fact =1;
void fun(int x){

	if(x==0){
		return;
	}
	fact*=x--;
	fun(x);
}

void main(){

	print("forloop");
	int f = 1;
	for(int i=6;i>0;i--){
		f=f*i;
	}

	print(f);
	
	print("by recursion");
	fun(6);
	print(fact);

}
