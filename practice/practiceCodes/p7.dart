class Demo{

	final int x;
	final String str;

	const Demo({ required this.x, required this.str});

	void printData(){
		print(x);
		print(str);
	}	
}
/*
class Demo{

        final int? x;
        final String?
 str;

        const Demo({this.x,this.str});

        void printData(){
                print(x);
                print(str);
        }
}
*/
void main(){

	Demo obj = Demo(str:"Mayur",x:10);
	obj.printData();
}
