class Demo{

	Demo(){

		print("In Constructor");
	}		
	
	void fun(){

		print("In Fun");
	}
}

Demo objFun(){
	print("In objFun");	
	return Demo();
}

void main(){
	
	Demo obj = objFun();
	obj.fun();
}
