class Demo{

	final int x;
	final String str;

	const Demo({required this.x,required this.str});		
	
	void fun(){

		print("In Fun");
		print(x);
		print(str);
	}
}

Demo objFun(){
	print("In objFun");	
	return Demo(x:10,str:"MAYUR");
}

void main(){
	
	Demo obj = objFun();
	obj.fun();
}
