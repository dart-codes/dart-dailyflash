class Demo{

	void data(){

		print("In data");
	}

	void career(){
		print("pharmacist");
	}
}
class DemoChild extends Demo{

	void career(){
		print("Engineer...");
	}
}

void main(){

	Demo obj = DemoChild();
	obj.data();
	obj.career();
}
