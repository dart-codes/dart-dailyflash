class StatelessWidget{

	StatelessWidget(){
		print("In stateless widget Constructor...");
	}
	Widget build(BuildContext context){
		print("In statlessWidget build");
		return MaterialApp();
	}
}

class MyApp extends StatelessWidget{

	MyApp(){

		print("In MyApp Constructor...");
	}
	@override
	Widget build(BuildContext context){

		return MaterialApp();
	}
}
class MaterialApp extends Widget{

	MaterialApp(){
		print("In Material App Consturctor...");
	}	
}

class BuildContext{

	BuildContext(){
		print("In BuildContext Constructor...");
	}
}

class Widget{

	Widget(){
		print("In Widget Constructor...");
	}
}

void main(){

	MyApp obj = MyApp();
	BuildContext obj1 = BuildContext();
	obj.build(obj1);
}
