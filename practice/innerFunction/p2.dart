
Function Outer(){

	print("Outer Functions");
	void Inner(){
		print("Inner Functions");
	}

	return Inner;
}

class Demo{

	void fun(){

	}
}
void main(){

	Function ret = Outer();
	ret();
	Demo obj = new Demo();
	print(ret.runtimeType);
	print(Outer.runtimeType);
	print(obj.runtimeType);
	print(obj.fun.runtimeType);
}
