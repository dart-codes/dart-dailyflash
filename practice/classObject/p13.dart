

class Company{

	int? empCount;
	String? compName;
//type 1
/*	Company(int empCount, String compName){    //basic type  default constructor


		this.empCount = empCount;
		this.compName = compName;
	}
*/

//type2
//	Company(this.empCount,this.compName);     //constructor type 1.  complusory parameter.


//type3
//	Company(this.empCount,[this.compName = "vishwakarma"]);// contructor type 2. with optional parameter.

//type4
//	Company(this.empCount,{this.compName = "Vishwakarma"}); //constructor type3. with default parameter.

//type 5 
	Company({this.empCount, this.compName});  //constructor type4 . with named parameter.

	void compInfo(){
	
		print(empCount);
		print(compName);
	}
}

void main(){

	Company obj1 = new Company(empCount:100,compName :"veritas");
	Company obj2 = new Company(compName :"pubmatic", empCount : 200);
	obj1.compInfo();
	obj2.compInfo();

}
