class Player{

	int? jerNo;
	String? pName;

	Player(this.jerNo, this.pName);
	
	void playerInfo(){

		print(jerNo);
		print(pName);
	}
}

void main(){

	Player obj1 = new Player(1,"KL Rahul");
	obj1.playerInfo();

	obj1.jerNo = 07;
	obj1.pName = "MS Dhoni";

	obj1.playerInfo();
}
