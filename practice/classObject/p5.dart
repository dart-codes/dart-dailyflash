
class Emp{

	int empId = 10;
	String empName = "mayur";
	double salary = 2200.34;

	void info(){

		print(empId);
		print(empName);
		print(salary);
	}
}

void main(){

	Emp obj1 = new Emp();
	obj1.info();
	
	Emp obj2 = Emp();
	obj2.info();
	print("===============");
	obj1.salary = 2400.43;
	obj1.info();
}
