import 'dart:io';

class Employee{

	int empId = 15;
	String? empName = "mayur";
	double sal = 1.0;

	void employeeInfo(){

		print("Employee Id = $empId");
		print("Employee Name = $empName");
		print("Employee Salary  = $sal");

	}
}

void main(){

	Employee emp1 = new Employee();
	emp1.employeeInfo();
	print("enter employee id - ");
	emp1.empId = int.parse(stdin.readLineSync()!);
	print("enter employee name");
	emp1.empName = stdin.readLineSync();
	print("enter employee salary - ");
	emp1.sal = double.parse(stdin.readLineSync()!);
	emp1.employeeInfo();

}
