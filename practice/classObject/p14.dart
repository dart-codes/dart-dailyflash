class Emp{

	int? empId;
	String? empName;
/*
	Emp(){ 		//default - no args constructor.

		print("Default Constructor");		
	}	
	
	Emp(this.empId,this.empName); 		// parameterized Constructor
*/

	Emp(){		//default constructor

		print("Default");
	}

	Emp.constructor(int empId,String empName){ 	//parameterized consructor

		print("Parameterized");
	}
}

void main(){

	Emp obj1 = new Emp();
	Emp obj2 = new Emp.constructor(10,"mayur");

}
