import 'dart:io';

class Demo{

	int? x;
	String? str;

	Demo(int x,String str){
		
		this.x = x;
		this.str = str;
		print("In Parameterized Constructor");
		print(this);

	}
	void printData(){
		
		
		print(x);
		print(str);
		print(this.hashCode);
	}
}

void main(){

	Demo obj = new Demo(10,"mayur");
	obj.printData();
	print(obj.hashCode);
	print(identityHashCode(obj));
	
	Demo obj1= new Demo(10,"mayur");
        obj1.printData();
        print(obj1.hashCode);
        print(identityHashCode(obj1));
	
}

