class Demo{

	static int x = 10;
	static int y = 20;

	static void printData(){
	
		print(x);
		print(y);

	}
}

void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo();
	Demo.printData();
	Demo.x = 50;
	Demo.printData();

}
