import 'dart:io';
class Player{

	final int? jerNo;
	final String? playerName;

	const Player(this.jerNo, this.playerName);

}


void main(){
	
	Player obj1 = const Player(10,"mayur");
	print(obj1.hashCode);

	Player obj2 = const Player(10,"mayur");
	print(obj2.hashCode);

}
