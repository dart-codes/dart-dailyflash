class parent{

	int x = 10;
	parent(){
	
		System.out.println("Parent constructor");
		System.out.println(this);
	}
	void printdata(){
	
		System.out.println(x);
	}
}
class child extends parent{

	int x =20;
	child(){
	
		System.out.println("Child constructor");
		System.out.println(this);
	}
	void dispData(){
	
		System.out.println(x);
	}
}
class client{

	public static void main(String[] args){
	
		child obj = new child();
		obj.printdata();
		obj.dispData();
	}
}
