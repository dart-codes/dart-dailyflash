class Parent{

	int x = 10;
	Parent(){
		print("Parent Constructor");
		print(this.hashCode);
	}
	void printData(){

		print(x);        
	}
}
class child extends Parent{

        int x = 20;
        child(){
                print("Child Constructor");
		print(this.hashCode);
        }
        void dispData(){

                print(x);
		print(super.x);
        }
}

void main(){

	child obj = new child();
	obj.printData();
	obj.dispData();
}
