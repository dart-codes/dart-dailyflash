class Parent{

	Parent(){
		print("Parent constructor");
	}
	call(){
		print("In parent method call");
	}
}	
class Child extends Parent{

        Child(){
		super();	//error
		print("Child constructor");
        }/*
	call(){
		print("In child method call");
	}	*/
}

void main(){

	Child obj = new Child();
	obj();
}

