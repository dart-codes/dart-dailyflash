class ICC{

	ICC(){
		print("ICC constructor");
	}
	
}

class BCCI extends ICC{

	BCCI(){
		print("BCCI Constructor");
	}
}

class IPL extends ICC{

	IPL(){
		print("IPL constructor");
	}
}
void main(){

	ICC obj1 = new ICC();
	BCCI obj2 = new BCCI();
	IPL obj = new IPL();
}
