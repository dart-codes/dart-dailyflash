class Parent{

	int x = 10;
	String str = "Pooja";

	Parent(){

		print("Parent Constructor");
	}
	void parentMethod(){
		
		print(x);
		print(str);
	}
}

class Child extends Parent{

	int x = 20;
	String str = "only Pooja";

	Child(){

		print("Child Constructor");
	}	
	
	void childMethod(){
		
		print(x);
		print(str);
	}
}

void main(){
	Parent obj = new Parent();
	Child obj1 = new Child();

	print(obj1.x);
	print(obj1.str);
	obj1.parentMethod();
	obj.parentMethod();
/*
	print(obj1.x);
	print(obj1.str);
	obj1.childMethod();	
*/	 
}
